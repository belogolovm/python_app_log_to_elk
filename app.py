import logging
import json
from datetime import datetime
print("enter the number:")
input1 = input()

s=int(input1)*2
print(s)

now = datetime.now()

year = now.strftime("%Y")
month = now.strftime("%m")
day = now.strftime("%d")
time = now.strftime("%H:%M:%S")

date=day+'.'+month+'.'+year

class JSONFormatter(logging.Formatter):
        def __init__(self):
                super().__init__()
        def format(self, record):
                record.msg = json.dumps(record.msg)
                return super().format(record)
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
loggingStreamHandler = logging.StreamHandler()
loggingStreamHandler = logging.FileHandler("log.json",mode='a')
loggingStreamHandler.setFormatter(JSONFormatter())
logger.addHandler(loggingStreamHandler)
logger.info({"date":date,"time":time,"number":s})
